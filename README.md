
Fichier README pour le projet startup
=====================================================

Ce fichier peut-être rédiger au format Markdown.

Markdown est un langage de balisage léger. Son but est d'offrir une syntaxe facile à lire et à écrire. Un document formaté selon Markdown devrait pouvoir être publié comme tel, en texte, sans donner l’impression qu’il a été marqué par des balises ou des instructions de formatage. Un document rédigé en Markdown peut être converti facilement en HTML.

  - Editeur en ligne : http://dillinger.io/
  - Documentation officiel : https://daringfireball.net/projects/markdown/syntax
  - Documentation de Bitbucket : https://confluence.atlassian.com/stash/markdown-syntax-guide-312740094.html#notfound
  - Documentation de GitHub : https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

