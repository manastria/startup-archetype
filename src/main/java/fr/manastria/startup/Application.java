package fr.manastria.startup;

import fr.manastria.utils.Console;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		logger.debug("main({})", args);

		Console.println("Test : {}", "rrr");
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
